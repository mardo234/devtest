﻿using BLL.Business.PriceLogic;
using BLL.Interfaces;
using BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Helpers
{
    public static class ToFromAndFromTo
    {
        public static List<ProductWithTimeSpan> ProductsToProductsWithTimeSpan(this IEnumerable<Product> products)
        {
            if (products != null)
            {
                List<ProductWithTimeSpan> productsWithTimeSpan = new List<ProductWithTimeSpan>();
                foreach (var item in products)
                {
                    var productWithTimeSpan = new ProductWithTimeSpan();
                    productWithTimeSpan.Id = item.Id;
                    productWithTimeSpan.Name = item.Name;
                    productWithTimeSpan.TimeSpan = 0;
                    productWithTimeSpan.Type = item.Type;
                    productsWithTimeSpan.Add(productWithTimeSpan);
                }
                return productsWithTimeSpan;
            }
            return null;
        }
        public static ProductWithTimeSpan ProductToProductWithTimeSpan(this Product product)
        {
            if (product != null)
            {
                ProductWithTimeSpan productWithTimeSpan = new ProductWithTimeSpan();
                productWithTimeSpan.Id = product.Id;
                productWithTimeSpan.Name = product.Name;
                productWithTimeSpan.TimeSpan = 0;
                productWithTimeSpan.Type = product.Type;
                return productWithTimeSpan;
            }
            return null;
        }
    }

}
