﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Business.PriceLogic
{
    public class HeavyPrice : RegularPrice
    {
        //rental price is one-time rental fee plus premium fee for each day rented.
        public HeavyPrice(int timeSpan) : base(timeSpan)
        {
        }

        public override double CalculatePrice()
        {
            var fee = rentalFee;
            fee += TimeSpan * premiumDailyFee;
            return fee;
        }
    }
}
