﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Business.PriceLogic
{
    public class SpecializedPrice : RegularPrice
    {
        //rental price is premium fee for the first 3 days plus regular fee times the number of days over 3.
        public SpecializedPrice(int timeSpan) : base(timeSpan)
        {
        }

        public override double CalculatePrice()
        {
            if (TimeSpan <= 3)
            {
                return 3 * premiumDailyFee;
            }
            else
            {
                var newTimeSpan = TimeSpan - 3;
                return ((3 * premiumDailyFee) + (newTimeSpan * regularDailyFee));
            }
            return 2.0;
        }
    }
}
