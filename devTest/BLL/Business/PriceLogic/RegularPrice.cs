﻿using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Business.PriceLogic
{
    public class RegularPrice : IProductPrice
    {
        // rental price is one-time rental fee plus premium fee for the first 2 days plus regular fee for the number of days over 2.
        public int TimeSpan { get; set; }
        protected double rentalFee = 100.00;
        protected double regularDailyFee = 40.00;
        protected double premiumDailyFee = 60.00;

        public RegularPrice(int timeSpan)
        {
            TimeSpan = timeSpan;
        }

        public virtual double CalculatePrice()
        {
            double fee = rentalFee;
            if (TimeSpan <= 2)
            {
                fee += premiumDailyFee * TimeSpan;
            }
            else
            {
                fee += premiumDailyFee * 2;
                var newTimeSpan = TimeSpan - 2;
                fee += newTimeSpan * regularDailyFee;
            }
            return fee;
        }
    }
}
