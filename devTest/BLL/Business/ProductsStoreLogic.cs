﻿using BLL.Interfaces;
using BLL.Models;
using BLL.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Repos;

namespace BLL.Business
{
    public class ProductsStoreLogic : IProductsStoreLogic
    {
        private readonly IProductsRepo _productsRepo;
        private readonly ITempRepo _tempRepo;
        public ProductsStoreLogic(IProductsRepo productsRepo, ITempRepo tempRepo)
        {
            _productsRepo = productsRepo;
            _tempRepo = tempRepo;
        }

        public List<ProductWithTimeSpan> GetStoredProductsWithAllProducts()
        {
            var products = _productsRepo.GetAll().ProductsToProductsWithTimeSpan();
            products = products.Concat(_tempRepo.GetAllStoredProducts()).GroupBy(x => x.Id).Select(x => x.Last()).ToList();
            return products;
        }

        public Invoice GetStoredProductsInvoice()
        {
            List<ProductWithPrice> pricedProducts = new List<ProductWithPrice>();
            double totalPrice = 0.0;
            int totalPoints = 0;
            var storedProducts = _tempRepo.GetAllStoredProducts();
            if (storedProducts == null) return null;
            foreach (var product in storedProducts)
            {
                ProductWithPrice productWithPrice = new ProductWithPrice(product.Id, product.Name, product.Type, product.TimeSpan);
                pricedProducts.Add(productWithPrice);
                totalPoints += productWithPrice.Points;
                totalPrice += productWithPrice.Price;
            }
            Invoice invoice = new Invoice();
            invoice.PricedProducts = pricedProducts;
            invoice.TotalPrice = totalPrice;
            invoice.TotalPoints = totalPoints;
            return invoice;
        }

        public int GetAddedToCartProductsCount()
        {
            return _tempRepo.GetAllStoredProducts().Count();
        }

        public void StoreProduct(ProductWithTimeSpan product)
        {
            if (product != null && product.TimeSpan > 0)
            {
                _tempRepo.StoreProduct(product);
            }
        }

        public ProductWithTimeSpan GetProductWithTimeSpan(int idOfProduct)
        {
            if (_tempRepo.GetAllStoredProducts().Any(x => idOfProduct.Equals(x.Id))) return _tempRepo.GetAllStoredProducts().Where(x => idOfProduct.Equals(x.Id)).FirstOrDefault();

            return _productsRepo.GetProductById(idOfProduct).ProductToProductWithTimeSpan();
        }

    }
}
