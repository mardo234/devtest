﻿using BLL.Models;
using System.Collections.Generic;

namespace BLL.Interfaces
{
    public interface ITempRepo
    {
        IEnumerable<ProductWithTimeSpan> GetAllStoredProducts();
        void StoreProduct(ProductWithTimeSpan product);
    }
}
