﻿using BLL.Business.PriceLogic;
using BLL.Interfaces;
using System;

namespace BLL.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }

    public class ProductWithTimeSpan : Product
    {
        public int TimeSpan { get; set; }
    }
    public class ProductWithPrice : ProductWithTimeSpan
    {
        public double Price { get; private set; }
        public int Points { get; set; }

        public ProductWithPrice(int id, string name, string type, int timeSpan)
        {
            this.Id = id;
            this.Name = name;
            this.Type = type;
            this.TimeSpan = timeSpan;
            SetPrice();
            SetLoyaltyPoints();
        }

        public void SetPrice()
        {
            if (this != null && !String.IsNullOrEmpty(this.Type))
            {
                IProductPrice priceLogic = null;
                switch (this.Type.ToLower())
                {
                    case "heavy": priceLogic = new HeavyPrice(this.TimeSpan); break;
                    case "specialized": priceLogic = new SpecializedPrice(this.TimeSpan); break;
                    default: priceLogic = new RegularPrice(this.TimeSpan); break;
                }
                Price = priceLogic.CalculatePrice();
            }
        }

        public void SetLoyaltyPoints()
        {
            if (this != null && !String.IsNullOrEmpty(this.Type))
            {
                switch (this.Type.ToLower())
                {
                    case "heavy": Points = 2; break;
                    default: Points = 1; break;
                }
            }
        }
    }
}
