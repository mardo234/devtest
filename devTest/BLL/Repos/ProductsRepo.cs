﻿using BLL.Interfaces;
using BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Repos
{
    public class ProductsRepo : IProductsRepo
    {
        private List<Product> products = new List<Product>();
        public ProductsRepo()
        {
            products.Add(new Product { Id = GetUniqueId(), Name = "Caterpillar bulldozer", Type = "Heavy" });
            products.Add(new Product { Id = GetUniqueId(), Name = "KamAZ truck", Type = "Regular" });
            products.Add(new Product { Id = GetUniqueId(), Name = "Komatsu crane", Type = "Heavy" });
            products.Add(new Product { Id = GetUniqueId(), Name = "Volvo steamroller", Type = "Regular" });
            products.Add(new Product { Id = GetUniqueId(), Name = "Bosch jackhammer", Type = "Specialized" });
        }

        private int GetUniqueId()
        {
            Product p = products.LastOrDefault();
            if (p == null) return 1;
            return p.Id + 1;
        }

        public IEnumerable<Product> GetAll()
        {
            return products;
        }

        public Product GetProductById(int id)
        {
            return products.Where(x => id.Equals(x.Id)).FirstOrDefault();
        }
    }
}
