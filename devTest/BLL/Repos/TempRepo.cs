﻿using BLL.Interfaces;
using BLL.Models;
using System.Collections.Generic;

namespace BLL.Repos
{
    public class TempRepo:ITempRepo
    {
        private static List<ProductWithTimeSpan> storedProducts = new List<ProductWithTimeSpan>();

        public IEnumerable<ProductWithTimeSpan> GetAllStoredProducts()
        {
            return storedProducts;
        }

        public void StoreProduct(ProductWithTimeSpan product)
        {
            storedProducts.Add(product);
        }
    }
}
