﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class IndexVM
    {
        [MinLength(0)]
        public int ItemsAddedToCartCount { get; set; }
        public IEnumerable<ProductVM> Products { get; set; }
    }

    public class ProductVM
    {
        [Required]
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        [MinLength(0)]
        public int TimeSpan { get; set; }
    }


    public class AddToCartVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int TimeSpan { get; set; }
    }
}