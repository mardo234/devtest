﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Helpers;
using System.Text;
using BLL.Models;
using BLL.Interfaces;
using BLL.Repos;
using BLL.Business;
using Web.Properties;

namespace Web.Controllers
{
    public class StoreController : Controller
    {
        private readonly IProductsStoreLogic _productsLogic;
        public StoreController()
        {
            var productsRepo = new ProductsRepo();
            var tempRepo = new TempRepo();
            _productsLogic = new ProductsStoreLogic(productsRepo, tempRepo);
        }

        public ActionResult Index()
        {
            IndexVM vm = new IndexVM();
            vm.ItemsAddedToCartCount = _productsLogic.GetAddedToCartProductsCount();
            vm.Products = _productsLogic.GetStoredProductsWithAllProducts().ProductsWithTimeSpanToProductVMs();
            return View(vm);
        }

        public ActionResult AddToCart(int id)
        {
            var product = _productsLogic.GetProductWithTimeSpan(id);
            if (product == null) return RedirectToAction("Index");
            return View(product.ProductWithTimeSpanToAddToCartVM());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddToCart(AddToCartVM vm)
        {
            if (ModelState.IsValid && vm != null)
            {

                _productsLogic.StoreProduct(vm.AddToCartVMToProductWithTimeSpan());
            }
            return RedirectToAction("Index");
        }

        public ActionResult GetInvoice()
        {
            var invoiceTemplate = Resources.InvoiceTemplate;
            StringBuilder sb = new StringBuilder();
            int i = 1;
            var invoice = _productsLogic.GetStoredProductsInvoice();
            if (invoice == null || invoice.PricedProducts == null) return RedirectToAction("Index", new { err = 1 });
            foreach (var item in invoice.PricedProducts)
            {
                sb.AppendLine(string.Format("{0}.\t{2}\t\t{3}\t\t{1}", i, item.Name, item.TimeSpan, item.Price));
                i++;
            }
            invoiceTemplate = invoiceTemplate.Replace("{products}", sb.ToString());
            invoiceTemplate = invoiceTemplate.Replace("{totalSum}", invoice.TotalPrice.ToString());
            invoiceTemplate = invoiceTemplate.Replace("{pointsEarned}", invoice.TotalPoints.ToString());
            return File(Encoding.UTF8.GetBytes(invoiceTemplate),
                 "text/plain",
                  string.Format("invoice_{0}.txt", DateTime.Now.ToString("yyyyMMddTHHmmss")));
        }
    }
}
