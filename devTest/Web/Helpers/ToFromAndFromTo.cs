﻿using BLL.Models;
using System.Collections.Generic;
using Web.Models;

namespace Web.Helpers
{
    public static class ToFromAndFromTo
    {
        public static List<ProductVM> ProductsWithTimeSpanToProductVMs(this IEnumerable<ProductWithTimeSpan> products)
        {
            List<ProductVM> vms = new List<ProductVM>();
            if (products != null)
            {
                foreach (var product in products)
                {
                    ProductVM vm = new ProductVM();
                    vm.Id = product.Id;
                    vm.Name = product.Name;
                    vm.Type = product.Type;
                    vm.TimeSpan = product.TimeSpan;
                    vms.Add(vm);
                }
            }
            return vms;
        }

        public static ProductWithTimeSpan AddToCartVMToProductWithTimeSpan(this AddToCartVM vm)
        {
            if (vm != null)
            {
                var product = new ProductWithTimeSpan();
                product.Id = vm.Id;
                product.Name = vm.Name;
                product.Type = vm.Type;
                product.TimeSpan = vm.TimeSpan;
                
                return product;
            }
            return null;
        }

        public static AddToCartVM ProductWithTimeSpanToAddToCartVM(this ProductWithTimeSpan product)
        {
            if (product!=null)
            {
                var vm = new AddToCartVM();
                vm.Id = product.Id;
                vm.Name = product.Name;
                vm.TimeSpan = product.TimeSpan;
                vm.Type = product.Type;
                return vm;
            }
            return null;
        }
    }
}